# -*- coding: utf-8 -*-
from os.path import join

from django.conf import settings
from django.test import TestCase
from django.utils.translation import ugettext as _
from django.forms import FileField, ChoiceField
from django.core.files.uploadedfile import SimpleUploadedFile

from ranking_app.models import Event
from ranking_app.forms import RankingUploadForm

from model_mommy import mommy

m = mommy.make_one

class RankingUploadFormTestCase(TestCase):

    def setUp(self):
        self.event = m(Event, name='Corrida do Saco')
        self.form = RankingUploadForm()

    def test_fields(self):
        self.assertItemsEqual(['csv_file', 'event'], self.form.fields)

    def test_csv_file_field(self):
        self.assertIsInstance(self.form.fields['csv_file'], FileField)
        self.assertEqual(u'Arquivo CSV', self.form.fields['csv_file'].label)
        self.assertTrue(self.form.fields['csv_file'].required)

    def test_event_field(self):
        self.assertIsInstance(self.form.fields['event'], ChoiceField)
        self.assertEqual(u'Etapa', self.form.fields['event'].label)
        self.assertTrue(self.form.fields['event'].required)

    def test_csvfile_field_only_accepts_csv_mimetypes(self):
        jpg_file = open(join(settings.MEDIA_ROOT, 'test/test1.jpg'))
        form = RankingUploadForm({
            'event': self.event.id,
            'is_worldwide': False,
        }, {
            'csv_file': SimpleUploadedFile(jpg_file.name, jpg_file.read()),
        })
        jpg_file.close()
        self.assertFalse(form.is_valid())
        self.assertIn('csv_file', form.errors)

    def test_successful(self):
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test1.csv'))
        form = RankingUploadForm({
            'event': self.event.id,
            'is_worldwide': False,
        }, {
            'csv_file': SimpleUploadedFile(csv_file.name, csv_file.read()),
        })
        csv_file.close()
        self.assertTrue(form.is_valid())
