from django.conf.urls import patterns, include, url
from django.contrib.auth.views import login, logout

from .views import (home, upload, update_ranking, update_successful, ranking,
                    ranking_html, upload_history, all_rankings)

urlpatterns = patterns('',
    url(r'^$', home, name="home"),
    url(r'^upload/$', upload, name="upload"),
    (r'^login/$', 'django.contrib.auth.views.login',
                   {'template_name': 'login.html'}),
    (r'^logout/$', 'django.contrib.auth.views.logout',
                   {'template_name': 'logged_out.html'}),
    url(r'^update_ranking/$', update_ranking, name='update_ranking'),
    url(r'^upload_successful/(?P<id>\d+)/$', update_successful, name='update_successful'),
    url(r'^ranking_json/(?P<modality_type>\w+)/(?P<gender>[M,F]{1,1})/(?P<category>\w)/$', ranking, name='ranking'),
    url(r'^ranking/(?P<modality_type>\w+)/(?P<gender>[M,F]{1,1})/(?P<category>\w)/$',
        ranking_html, name='ranking_html'),
    url(r'^history/$', upload_history, name='upload_history'),
    url(r'^modality/(?P<modality_type>\w+)/gender/(?P<gender>\w{1})/$', all_rankings, name="all_rankings")
)
