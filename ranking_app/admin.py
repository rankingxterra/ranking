from django.contrib import admin

from .models import *

admin.site.register(Modality)
admin.site.register(Event)
admin.site.register(EventType)
admin.site.register(CsvUploadedFile)
admin.site.register(Category)
admin.site.register(Athlete)
admin.site.register(AthleteEventPosition)
admin.site.register(PositionPontuation)
