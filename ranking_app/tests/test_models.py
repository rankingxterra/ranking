# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models
from django.test import TestCase
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

from model_mommy import mommy

from ranking_app.models import *

m = mommy.make_one
mr = mommy.make_recipe


class ModalityModelTestCase(TestCase):

    def setUp(self):
        self.model = Modality
        self.instance = m(self.model, name='Duathlon/Triathlon')

    def test_unicode(self):
        self.assertEqual('Duathlon/Triathlon', unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Modalidade',self.model._meta.verbose_name)
        self.assertEqual('Modalidades',self.model._meta.verbose_name_plural)


class EventTypeModelTestCase(TestCase):

    def setUp(self):
        self.model = EventType
        self.instance = m(self.model, name='Mundial')

    def test_unicode(self):
        self.assertEqual('Mundial', unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Tipo de Etapa',self.model._meta.verbose_name)
        self.assertEqual('Tipos de Etapa',self.model._meta.verbose_name_plural)


class EventModelTestCase(TestCase):

    def setUp(self):
        self.model = Event
        self.instance = m(self.model, name='Corrida do Saco')

    def test_unicode(self):
        self.assertEqual('Corrida do Saco', unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Etapa',self.model._meta.verbose_name)
        self.assertEqual('Etapas',self.model._meta.verbose_name_plural)


class UploadedCsvFileModelTestCase(TestCase):

    def setUp(self):
        self.model = CsvUploadedFile
        self.instance = m(self.model, event__name='Corrida do Saco')

    def test_unicode(self):
        self.assertEqual('%s for Corrida do Saco' % self.instance.csv_file.name, unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Upload de Arquivo',self.model._meta.verbose_name)
        self.assertEqual('Uploads de Arquivo',self.model._meta.verbose_name_plural)


class CategoryModelTestCase(TestCase):
    def setUp(self):
        self.model = Category
        self.instance = m(self.model, name='Profissional Bolado')

    def test_unicode(self):
        self.assertEqual(u'Profissional Bolado',  unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Categoria',self.model._meta.verbose_name)
        self.assertEqual('Categorias',self.model._meta.verbose_name_plural)

    def test_has_athlete(self):
        modality = m(Modality)
        athlete = m(Athlete, category=self.instance, disqualified=False)
        athlete_position = m(AthleteEventPosition, athlete=athlete, event__type__modality=modality)

        self.assertTrue(self.instance.has_athlete(gender=athlete.gender, modality=modality))

    def test_has_athlete_is_False_for_no_athletes_in_modality(self):
        modality = m(Modality)
        athlete = m(Athlete, category=self.instance, disqualified=False)

        self.assertFalse(self.instance.has_athlete(gender=athlete.gender, modality=modality))

class AthleteModelTestCase(TestCase):
    def setUp(self):
        self.model = Athlete
        self.instance = m(self.model, name='Jalim Rabei')

    def test_unicode(self):
        self.assertEqual('Jalim Rabei: %s' % self.instance.xterra_id, unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual('Atleta',self.model._meta.verbose_name)
        self.assertEqual('Atletas',self.model._meta.verbose_name_plural)

    def test_total_points_property(self):
        event1 = m(Event)
        pos1 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=1)
        pos2 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=6)
        pos3 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=3)
        pos3 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=4)

        position_pontuation1 = m(PositionPontuation, event_type=event1.type, position=1, pontuation=10)
        position_pontuation2 = m(PositionPontuation, event_type=event1.type, position=2, pontuation=9)
        position_pontuation3 = m(PositionPontuation, event_type=event1.type, position=3, pontuation=8)
        position_pontuation4 = m(PositionPontuation, event_type=event1.type, position=4, pontuation=7)
        position_pontuation5 = m(PositionPontuation, event_type=event1.type, position=5, pontuation=6)
        position_pontuation6 = m(PositionPontuation, event_type=event1.type, position=6, pontuation=5)

        event_points, total_points = self.instance.total_points([event1],
                                                                event1.type)

        self.assertEqual(total_points, 7)

    def test_total_points_property_is_zero_for_no_points(self):
        event1 = m(Event)

        event_points, total_points = self.instance.total_points([event1],
                                                                event1.type)

        self.assertEqual(total_points, 0)

    def test_total_points_for_different_events1(self):
        event_type = m(EventType)
        event1 = m(Event, type=event_type)
        event2 = m(Event, type=event_type)
        event3 = m(Event, type=event_type)
        pos1 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=1)
        pos2 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event2),
                 event=event2,
                 position=6)
        pos3 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event3),
                 event=event3,
                 position=3)

        position_pontuation1 = m(PositionPontuation, event_type=event1.type, position=1, pontuation=10)
        position_pontuation3 = m(PositionPontuation, event_type=event3.type, position=3, pontuation=8)
        position_pontuation6 = m(PositionPontuation, event_type=event2.type, position=6, pontuation=5)

        event_points, total_points = self.instance.total_points([event1, event2, event3],
                                                                event_type)

        self.assertEqual(total_points, 23)

    def test_total_points_for_different_events(self):
        event_type = m(EventType)
        event1 = m(Event, type=event_type)
        event2 = m(Event, type=event_type)
        event3 = m(Event, type=event_type)
        pos1 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=2)
        pos2 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event2),
                 event=event2,
                 position=5)
        pos3 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event3),
                 event=event3,
                 position=8)
        pos11 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event1),
                 event=event1,
                 position=1)
        pos21 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event2),
                 event=event2,
                 position=6)
        pos31 = m(AthleteEventPosition,
                 athlete=self.instance,
                 upload=m(CsvUploadedFile, event=event3),
                 event=event3,
                 position=3)

        position_pontuation1 = m(PositionPontuation, event_type=event1.type, position=1, pontuation=10)
        position_pontuation3 = m(PositionPontuation, event_type=event3.type, position=3, pontuation=9)
        position_pontuation6 = m(PositionPontuation, event_type=event2.type, position=6, pontuation=7)
        position_pontuation2 = m(PositionPontuation, event_type=event1.type, position=2, pontuation=10)
        position_pontuation5 = m(PositionPontuation, event_type=event3.type, position=5, pontuation=8)
        position_pontuation8 = m(PositionPontuation, event_type=event2.type, position=8, pontuation=5)

        event_points, total_points = self.instance.total_points([event1, event2, event3],
                                                                event_type)

        self.assertEqual(total_points, 26)


class AthleteEventPositionModelTestCase(TestCase):
    def setUp(self):
        self.model = AthleteEventPosition
        self.instance = m(self.model)

    def test_unicode(self):
        self.assertEqual('%s: %s #%d' % (self.instance.event.name,
                                         self.instance.athlete.name,
                                         self.instance.position),
                         unicode(self.instance))

    def test_verbose_name(self):
        self.assertEqual(u'Posição de Atleta em Evento',self.model._meta.verbose_name)
        self.assertEqual(u'Posições de Atletas em Eventos',self.model._meta.verbose_name_plural)


class PositionPontuationTestCase(TestCase):
    def test_import(self):
        from ranking_app.models import PositionPontuation
