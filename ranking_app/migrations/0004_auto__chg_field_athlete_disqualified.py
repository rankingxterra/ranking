# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Athlete.disqualified'
        db.alter_column(u'ranking_app_athlete', 'disqualified', self.gf('django.db.models.fields.BooleanField')())

    def backwards(self, orm):

        # Changing field 'Athlete.disqualified'
        db.alter_column(u'ranking_app_athlete', 'disqualified', self.gf('django.db.models.fields.NullBooleanField')(null=True))

    models = {
        u'ranking_app.athlete': {
            'Meta': {'object_name': 'Athlete'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Category']"}),
            'disqualified': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'team_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'xterra_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '15'})
        },
        u'ranking_app.athleteeventposition': {
            'Meta': {'object_name': 'AthleteEventPosition'},
            'athlete': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Athlete']"}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'upload': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.CsvUploadedFile']"})
        },
        u'ranking_app.category': {
            'Meta': {'object_name': 'Category'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'ranking_app.csvuploadedfile': {
            'Meta': {'object_name': 'CsvUploadedFile'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'csv_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'errors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lines_processed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'processed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'ranking_app.event': {
            'Meta': {'object_name': 'Event'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.EventType']"})
        },
        u'ranking_app.eventtype': {
            'Meta': {'object_name': 'EventType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modality': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Modality']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'ranking_app.modality': {
            'Meta': {'object_name': 'Modality'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '3'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'ranking_app.positionpontuation': {
            'Meta': {'object_name': 'PositionPontuation'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.EventType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pontuation': ('django.db.models.fields.IntegerField', [], {}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['ranking_app']