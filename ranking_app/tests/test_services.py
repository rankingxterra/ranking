# -*- coding: utf-8 -*-
from os.path import join

from model_mommy import mommy
from django.test import TestCase
from django.conf import settings
from django.core.files import File

from ranking_app.models import (CsvUploadedFile, Athlete, AthleteEventPosition,
                                Event, EventType, Category, Modality, PositionPontuation)
from ranking_app.services import process_csv_upload, athletes_by_modality, apply_disqualification_rules

m = mommy.make_one
p = mommy.prepare_one
mm = mommy.make_many

class ProcessCsvTestCase(TestCase):

    def test_successful(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test1.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(20, Athlete.objects.count())
        self.assertEqual(0, self.instance.errors)
        self.assertTrue(self.instance.processed)


    def test_two_athletes_with_the_same_id(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test2.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(1, Athlete.objects.count())
        self.assertEqual(0, self.instance.errors)
        self.assertTrue(self.instance.processed)

    def test_two_athletes_with_the_same_id_with_the_same_category(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test3.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(1, Athlete.objects.count())
        self.assertEqual(0, self.instance.errors)
        self.assertTrue(self.instance.processed)
        self.assertFalse(Athlete.objects.all()[0].disqualified)

    def test_two_athletes_with_the_same_id_and_different_categories(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test2.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(1, Athlete.objects.count())
        self.assertEqual(0, self.instance.errors)
        self.assertTrue(self.instance.processed)
        self.assertTrue(Athlete.objects.all()[0].disqualified)

    def test_malformed_csv(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test4.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(2, self.instance.errors)
        self.assertEqual(0, self.instance.lines_processed)
        self.assertTrue(self.instance.processed)

    def test_error_for_unexistent_category(self):
        event = mommy.make_one(Event, name='Corrida do Saco')
        csv_file = open(join(settings.MEDIA_ROOT, 'test/test5.csv'))
        self.instance = m(CsvUploadedFile, event=event, csv_file=File(csv_file))
        csv_file.close()

        result = process_csv_upload(self.instance)
        self.assertEqual(1, Athlete.objects.count())
        self.assertEqual(1, self.instance.errors)
        self.assertTrue(self.instance.processed)


class TestQueryServices(TestCase):
    def test_athletes_by_event_type(self):
        category1 = Category.objects.get(abbreviation='M')
        category2 = Category.objects.get(abbreviation='E')
        category3 = Category.objects.get(abbreviation='N')

        self.athlete1 = m(Athlete, gender="M", category=category1)
        self.athlete2 = m(Athlete, gender="F", category=category2)
        self.athlete3 = m(Athlete, gender="M", category=category3)
        self.athlete4 = m(Athlete, gender="M", category=category1)
        self.athlete5 = m(Athlete, gender="F", category=category2)
        self.athlete6 = m(Athlete, gender="M", category=category3)
        self.athlete7 = m(Athlete, gender="M", category=category3)

        self.modality1 = m(Modality)
        self.modality2 = m(Modality)

        self.event_type1 = m(EventType, modality=self.modality1)
        self.event_type2 = m(EventType, modality=self.modality1)
        self.event_type3 = m(EventType, modality=self.modality2)

        self.event1 = m(Event, type=self.event_type1)
        self.event2 = m(Event, type=self.event_type2)
        self.event3 = m(Event, type=self.event_type3)

        m(AthleteEventPosition,
                 athlete=self.athlete1,
                 upload=m(CsvUploadedFile, event=self.event1),
                 event=self.event1,
                 position=2)
        m(AthleteEventPosition,
                 athlete=self.athlete2,
                 upload=m(CsvUploadedFile, event=self.event2),
                 event=self.event2,
                 position=5)
        m(AthleteEventPosition,
                 athlete=self.athlete3,
                 upload=m(CsvUploadedFile, event=self.event3),
                 event=self.event3,
                 position=8)
        m(AthleteEventPosition,
                 athlete=self.athlete4,
                 upload=m(CsvUploadedFile, event=self.event1),
                 event=self.event1,
                 position=1)
        m(AthleteEventPosition,
                 athlete=self.athlete5,
                 upload=m(CsvUploadedFile, event=self.event2),
                 event=self.event2,
                 position=6)
        m(AthleteEventPosition,
                 athlete=self.athlete6,
                 upload=m(CsvUploadedFile, event=self.event3),
                 event=self.event3,
                 position=3)
        m(AthleteEventPosition,
                 athlete=self.athlete7,
                 upload=m(CsvUploadedFile, event=self.event1),
                 event=self.event1,
                 position=100)

        m(PositionPontuation, pontuation=10, position=2, event_type=self.event_type1)
        m(PositionPontuation, pontuation=10, position=5, event_type=self.event_type2)
        m(PositionPontuation, pontuation=10, position=8, event_type=self.event_type3)
        m(PositionPontuation, pontuation=10, position=1, event_type=self.event_type1)
        m(PositionPontuation, pontuation=10, position=6, event_type=self.event_type2)
        m(PositionPontuation, pontuation=10, position=3, event_type=self.event_type3)

        self.assertItemsEqual([self.athlete1, self.athlete4],
                          athletes_by_modality(self.modality1,
                                               self.athlete1.gender,
                                               self.athlete1.category.abbreviation))

        self.assertNotIn(self.athlete7,
                         athletes_by_modality(self.modality1,
                                              self.athlete7.gender,
                                              self.athlete7.category.abbreviation))

        self.assertItemsEqual([self.athlete2, self.athlete5],
                          athletes_by_modality(self.modality1,
                                               self.athlete2.gender,
                                               self.athlete2.category.abbreviation))

        self.assertItemsEqual([self.athlete3, self.athlete6],
                          athletes_by_modality(self.modality2,
                                               self.athlete3.gender,
                                               self.athlete3.category.abbreviation))


class ApplyDisqualificationRules(TestCase):
    def test_triathlon_athlete_without_brazil_type_event_made(self):
        event_type1 = EventType.objects.get(name='XTERRA Brazil')
        event_type2 = EventType.objects.get(name='XTERRA Camp')
        athlete1 = m(Athlete, disqualified=False)
        athlete2 = m(Athlete, disqualified=False)
        position1 = m(AthleteEventPosition, athlete=athlete1, event__type=event_type1)
        position2 = m(AthleteEventPosition, athlete=athlete2, event__type=event_type2)
        apply_disqualification_rules()
        athlete1 = Athlete.objects.get(id=athlete1.id)
        athlete2 = Athlete.objects.get(id=athlete2.id)
        self.assertFalse(athlete1.disqualified)
        self.assertTrue(athlete2.disqualified)

    def test_triathlon_athlete_without_2_camp_type_events_made(self):
        event_type = EventType.objects.get(name='XTERRA Camp')
        events = mm(Event, type=event_type)
        athlete1 = m(Athlete, disqualified=False)
        athlete2 = m(Athlete, disqualified=False)
        position1 = m(AthleteEventPosition, athlete=athlete1, event=events[0])
        position2 = m(AthleteEventPosition, athlete=athlete1, event=events[1])
        position3 = m(AthleteEventPosition, athlete=athlete1, event=events[2])
        position4 = m(AthleteEventPosition, athlete=athlete2, event=events[0])
        apply_disqualification_rules()
        athlete1 = Athlete.objects.get(id=athlete1.id)
        athlete2 = Athlete.objects.get(id=athlete2.id)
        self.assertFalse(athlete1.disqualified)
        self.assertTrue(athlete2.disqualified)

    def test_triathlon_athlete_without_2_tour_type_events_made(self):
        event_type = EventType.objects.get(name='XTERRA Tour')
        events = mm(Event, type=event_type)
        athlete1 = m(Athlete, disqualified=False)
        athlete2 = m(Athlete, disqualified=False)
        position1 = m(AthleteEventPosition, athlete=athlete1, event=events[0])
        position2 = m(AthleteEventPosition, athlete=athlete1, event=events[1])
        position3 = m(AthleteEventPosition, athlete=athlete1, event=events[2])
        position4 = m(AthleteEventPosition, athlete=athlete2, event=events[0])
        apply_disqualification_rules()
        athlete1 = Athlete.objects.get(id=athlete1.id)
        athlete2 = Athlete.objects.get(id=athlete2.id)
        self.assertFalse(athlete1.disqualified)
        self.assertTrue(athlete2.disqualified)

    def test_endurance_athlete_without_2_events_made(self):
        modality = Modality.objects.get(abbreviation='END')

        events = mm(Event, 3, type__modality=modality)
        athlete1 = m(Athlete, disqualified=False)
        athlete2 = m(Athlete, disqualified=False)
        position1 = m(AthleteEventPosition, athlete=athlete1, event=events[0])
        position2 = m(AthleteEventPosition, athlete=athlete1, event=events[1])
        position3 = m(AthleteEventPosition, athlete=athlete1, event=events[2])
        position4 = m(AthleteEventPosition, athlete=athlete2, event=events[0])
        apply_disqualification_rules()
        athlete1 = Athlete.objects.get(id=athlete1.id)
        athlete2 = Athlete.objects.get(id=athlete2.id)
        self.assertFalse(athlete1.disqualified)
        self.assertTrue(athlete2.disqualified)
