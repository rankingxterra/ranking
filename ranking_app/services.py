# -*- coding: utf-8 -*-
import csv
import logging
from ranking_app.models import (AthleteEventPosition, Athlete, Category,
                                Modality, Event, EventType, PositionPontuation)


log = logging.getLogger(__name__)

def process_csv_upload(instance):
    with open(instance.csv_file.path) as csv_file:
        reader = RankingCSVReader(csv_file, instance)
        instance.errors = reader.errors
        instance.lines_processed = reader.lines_processed
        instance.processed = True
        return instance

def dec_and_enc(file_data, decode="latin-1", encode="utf-8"):
    for line in file_data.readlines():
        yield line.decode(decode).encode(encode)

class RankingCSVReader(object):

    def __init__(self, csv_file, instance):
        self.instance = instance
        self.event = instance.event
        self.reader = csv.DictReader(dec_and_enc(csv_file), delimiter=';')
        self.lines_processed = 0
        self.errors = 0
        self.process_reader()

    def process_reader(self):
        for item in self.reader:
            self._process_line(item)

    def _process_line(self, item):
        try:
            assert item[u'POSICAO']
            assert item[u'NOME']
            assert item[u'ID']
            assert item[u'SEXO']
            self.category = Category.objects.get(abbreviation=item['CATEGORIA'])

            athlete = Athlete.objects.filter(xterra_id=item['ID'])

            if athlete:
                athlete = athlete[0]
                self._is_qualified(athlete, item)

            else:
                athlete = self._create_athlete(item)

            AthleteEventPosition.objects.create(
                athlete=athlete,
                event=self.event,
                position=item[u'POSICAO'],
                upload=self.instance
            )
            self.lines_processed += 1
            log.info(u'Linha de CSV importada para atleta %s', athlete.name)

        except Exception as e:
            self.errors += 1
            log.error(u'Erro na importação da linha %s', item)

    def _is_qualified(self, athlete, item):
        if not(athlete.category == self.category):
            athlete.disqualified = True
            athlete.save()
            log.info(
                'Atleta %s desclassificado por mudança de categoria.',
                athlete.name
            )

    def _create_athlete(self, item):
        return Athlete.objects.create(name=item[u'NOME'],
                                      xterra_id=item[u'ID'],
                                      email= item.get(u'EMAIL'),
                                      gender=item[u'SEXO'],
                                      team_name=item[u'EQUIPE'],
                                      category=self.category,
                                      disqualified=False)

def athletes_by_modality(modality, gender, category):
    query_filter = {}
    query_filter['event__type__modality'] = modality
    query_filter['athlete__gender'] = gender
    query_filter['athlete__category__abbreviation'] = category

    athletes_positions = AthleteEventPosition.objects.filter(**query_filter)

    athletes = []
    for athlete_position in athletes_positions:
        position = athlete_position.position
        event_type = athlete_position.event.type
        pontuation = PositionPontuation.objects.filter(event_type=event_type, position=position)
        if pontuation and pontuation[0].pontuation:
            athlete = athlete_position.athlete
            athletes.append(athlete)

    return list(set(athletes))


def apply_disqualification_rules():
    modalities = Modality.objects.all()
    for modality in modalities:
        modality_athletes = Athlete.objects.filter(
            athleteeventposition__event__type__modality=modality
        ).distinct('id')
        modality_athletes.update(disqualified=False)
        if modality.abbreviation == 'TRI':
            event_types = EventType.objects.filter(modality=modality)
            for event_type in event_types:
                if event_type.name == 'XTERRA Brazil':
                    qualified_athletes = Athlete.objects.filter(
                        athleteeventposition__event__type=event_type)
                    if qualified_athletes:
                        for athlete in modality_athletes:
                            if athlete not in qualified_athletes:
                                athlete.disqualified = True
                                athlete.save()

                else:
                    positions = AthleteEventPosition.objects.filter(
                        event__type=event_type
                    ).distinct('event')
                    if len(positions) == 3:
                        for athlete in modality_athletes:
                            athlete_events = []
                            for event in [ position.event for position in positions ]:
                                position = AthleteEventPosition.objects.filter(athlete=athlete, event=event)
                                if position:
                                    athlete_events.append(position)
                            if len(athlete_events) < 2:
                                athlete.disqualified = True
                                athlete.save()

        elif modality.abbreviation == 'END':
            events = Event.objects.filter(type__modality=modality)

            positions = AthleteEventPosition.objects.filter(event__type__modality=modality).distinct('event')
            if len(positions) == 3:
                athletes = Athlete.objects.filter(athleteeventposition__in=positions)
                for athlete in athletes:
                    athlete_events = []
                    for event in [ position.event for position in positions ]:
                        position = AthleteEventPosition.objects.filter(athlete=athlete, event=event)
                        if position:
                            athlete_events.append(position)
                    if len(athlete_events) < 2:
                        athlete.disqualified = True
                        athlete.save()
