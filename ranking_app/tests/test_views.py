# -*- coding: utf-8 -*-
from os.path import join

from mock import Mock, patch

from django.conf import settings
from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.core.files.uploadedfile import SimpleUploadedFile

import simplejson

from ranking_app.models import (CsvUploadedFile, Event, Athlete, EventType,
                                AthleteEventPosition, PositionPontuation, Category,
                                Modality)

from model_mommy import mommy

m = mommy.make_one

class RankingTestMixin(object):
    def setup_users(self):
        from django.contrib.auth.models import User

        self.no_staff_user = User()
        self.no_staff_user.username = "no_staff"
        self.no_staff_user.set_password("no_staff")
        self.no_staff_user.save()

        self.user = User()
        self.user.username = "user"
        self.user.set_password("user")
        self.user.is_staff = True
        self.user.save()

class HomeViewTest(TestCase, RankingTestMixin):
    def test_home_get_anonymous(self):
        response = self.client.get(reverse('home'))

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('django.contrib.auth.views.login'))

    def test_home_get(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('home'))

        self.assertTrue('Atleta' in response.content)
        self.assertTrue('Categoria' in response.content)
        self.assertTrue('Modalidade' in response.content)
        self.assertTrue('Evento' in response.content)
        self.assertTrue('Tipos de Evento' in response.content)
        self.assertTrue('do Atleta em um Evento' in response.content)
        self.assertTrue('Pontos ganhos por' in response.content)


class UploadViewTest(TestCase, RankingTestMixin):
    def setUp(self):
        pass

    def test_upload_get_anonymous(self):
        response = self.client.get(reverse('upload'))

        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('django.contrib.auth.views.login'))

    def test_upload_get(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('upload'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "upload.html")

        self.assertContains(response, reverse('update_ranking'))
        self.assertContains(response, "event")
        self.assertContains(response, "csv_file")

    def test_upload_post(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.post(reverse('upload'))

        self.assertEqual(response.status_code, 405)
        self.assertTemplateNotUsed(response, "upload.html")


class UpdateRankingViewTest(TestCase, RankingTestMixin):
    def setUp(self):
        self.event = m(Event, name='Corrida do Saco')

        self.patcher = patch("django.contrib.messages.add_message")
        self.check_messages = self.patcher.start()

    def tearDown(self):
        self.patcher.stop()

    def test_update_ranking_get(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('update_ranking'))

        self.assertEqual(response.status_code, 405)
        self.assertTemplateNotUsed(response, "index.html")


    def test_update_ranking_post_successful(self):
        self.setup_users()
        self.client.login(username="user", password="user")

        csv_file = open(join(settings.MEDIA_ROOT, 'test/test1.csv'))
        response = self.client.post(reverse('update_ranking'), {
            'csv_file': csv_file,
            'event': self.event.id,
        })
        csv_file.close()

        self.assertTrue(CsvUploadedFile.objects.all()[0].processed)
        self.assertEqual(response.status_code, 302)

    def test_no_staff_upload(self):
        self.setup_users()
        self.client.login(username="no_staff", password="no_staff")

        csv_file = open(join(settings.MEDIA_ROOT, 'test/test1.csv'))
        response = self.client.post(reverse('update_ranking'), {
            'csv_file': csv_file,
            'event': self.event.id,
        }, follow=True)
        csv_file.close()

        self.assertRedirects(response, reverse('django.contrib.auth.views.login'))
        self.assertEqual(self.check_messages.call_args[0][2], "No permission")


class SuccessUploadTestCase(TestCase, RankingTestMixin):
    def setUp(self):
        self.instance = m(CsvUploadedFile)

    def test_success(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('update_successful', kwargs={'id': self.instance.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed("update_successful.html")
        self.assertIn('upload', response.context)
        self.assertEqual(self.instance, response.context['upload'])
        self.assertIn('models', response.context)

    def test_404(self):
        self.setup_users()
        self.client.login(username="user", password="user")
        response = self.client.get(reverse('update_successful', kwargs={'id': self.instance.id + 1}))
        self.assertEqual(response.status_code, 404)


class RankingTest(TestCase):
    def setUp(self):
        self.modality1 = m(Modality, name="Modality 1", abbreviation="Ma")
        self.modality2 = m(Modality, name="Modality 2", abbreviation="Mb")
        self.modality3 = m(Modality, name="Modality 3", abbreviation="Mc")
        self.event_type1 = m(EventType, name="Event Type 1", modality=self.modality1)
        self.event_type2 = m(EventType, name="Event Type 2", modality=self.modality2)
        self.event_type3 = m(EventType, name="Event Type 3", modality=self.modality3)
        self.athlete = m(Athlete, gender="M", category=Category.objects.get(abbreviation="E"),
                         team_name="team_name")
        self.event1 = m(Event, type=self.event_type1)
        self.event2 = m(Event, type=self.event_type2)
        self.event3 = m(Event, type=self.event_type3)
        self.pos1 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=m(CsvUploadedFile, event=self.event1),
                 event=self.event1,
                 position=2)
        self.pos2 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=m(CsvUploadedFile, event=self.event2),
                 event=self.event2,
                 position=5)
        self.pos3 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=m(CsvUploadedFile, event=self.event3),
                 event=self.event3,
                 position=8)
        self.upload1 = m(CsvUploadedFile, event=self.event1)
        self.pos11 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=self.upload1,
                 event=self.event1,
                 position=1)
        self.pos21 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=m(CsvUploadedFile, event=self.event2),
                 event=self.event2,
                 position=6)
        self.pos31 = m(AthleteEventPosition,
                 athlete=self.athlete,
                 upload=m(CsvUploadedFile, event=self.event3),
                 event=self.event3,
                 position=3)

        self.position_pontuation1 = m(PositionPontuation, event_type=self.event1.type, position=1, pontuation=10)
        self.position_pontuation2 = m(PositionPontuation, event_type=self.event1.type, position=2, pontuation=10)
        self.position_pontuation3 = m(PositionPontuation, event_type=self.event3.type, position=3, pontuation=9)
        self.position_pontuation5 = m(PositionPontuation, event_type=self.event3.type, position=5, pontuation=8)
        self.position_pontuation6 = m(PositionPontuation, event_type=self.event2.type, position=6, pontuation=7)
        self.position_pontuation8 = m(PositionPontuation, event_type=self.event2.type, position=8, pontuation=5)

    def test_ranking_get(self):
        response = self.client.get(reverse('ranking', args=['TRI', 'M', 'E']))

        self.assertEquals(response.status_code, 200)
        self.assertEquals(simplejson.loads(response.content), {"aaData": []})

        response = self.client.get(reverse('ranking', args=[self.event1.type.modality.abbreviation,
                                                            "M",
                                                            "E"]))

        expected = {"aaData": [{
            'name': self.athlete.name,
            'position': 1,
            'total_points': 10,
            'team_name': "team_name",
            'event_points': {
                str(self.event1.id): {
                    'event_name': self.event1.name,
                    'position': 1,
                    'pontuation': 10,
                    'created_at': self.upload1.created_at.strftime("%d/%m/%Y")
                }
            }
        }]}
        self.assertEquals(response.status_code, 200)
        self.assertEquals(simplejson.loads(response.content), expected)

class RankingHtmlTest(TestCase):
    def test_rankinghtml_get(self):
        modality1 = m(Modality, name="Modality 1", abbreviation="Ma")
        event_type = m(EventType, modality=modality1)
        response = self.client.get(reverse('ranking_html', args=[
            event_type.modality.abbreviation, "M", "E"
        ]))

        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed("ranking.html")
