# -*- coding: utf-8 -*-
import logging

from functools import wraps

import simplejson

from django.http import HttpResponse
from django.template import RequestContext
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.views.decorators.http import require_http_methods
from django.views.decorators.cache import cache_page

from ranking_app.forms import RankingUploadForm
from ranking_app.models import CsvUploadedFile, Athlete, Event, Modality, Category
from ranking_app import services

log = logging.getLogger(__name__)


def _rr(request, template_name, data):
    return render_to_response(template_name, data, RequestContext(request))

def staff_required(view):
    @wraps(view)
    def wrapped_view(request, *args, **kwargs):
        if request.user.is_staff:
            return view(request, *args, **kwargs)
        else:
            messages.add_message(request, messages.ERROR, _("No permission"))
            return redirect(reverse('django.contrib.auth.views.login'))
    return wrapped_view


@staff_required
@require_http_methods(['GET'])
def home(request):
    modalities = Modality.objects.all()
    return _rr(request, "index.html", {'modalities': modalities})


@staff_required
@require_http_methods(['GET'])
def upload(request):
    form = RankingUploadForm()
    return _rr(request, "upload.html", {'form': form})


@staff_required
@require_http_methods(['GET'])
def upload_history(request):
    uploads = CsvUploadedFile.objects.all()
    return _rr(request, "uploads.html", {'models': uploads})


@staff_required
@require_http_methods(['POST'])
def update_ranking(request):
    if request.method == "POST":
        form = RankingUploadForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save()
            instance = services.process_csv_upload(instance)
            instance.save()
            services.apply_disqualification_rules()
            return redirect(reverse('update_successful',
                                    kwargs={'id':instance.id}))
        else:
            return _rr(request, "index.html", {'form': form})


@staff_required
def update_successful(request, id):
    upload_instance = get_object_or_404(CsvUploadedFile, id=id)
    return _rr(request, "update_successful.html", {'upload': upload_instance,
                                                   'models': Athlete.objects.all()})


@cache_page(60 * 60)
@require_http_methods(['GET'])
def ranking(request, modality_type, gender, category):
    modality = Modality.objects.get(abbreviation=modality_type)
    events = Event.objects.filter(type__modality=modality)
    athletes = services.athletes_by_modality(modality, gender, category)
    athletes_list = []
    for athlete in athletes:
        event_points, total_points = athlete.total_points(events, modality)
        athletes_list.append({"name": athlete.name,
                              "event_points" : event_points,
                              "total_points" : total_points,
                              "position": 0,
                              "team_name": athlete.team_name})

    sort_by_total_points = lambda x, y: cmp(x['total_points'], y['total_points'])
    sorted_athletes = sorted(athletes_list, cmp=sort_by_total_points, reverse=True)

    for i, athlete in enumerate(sorted_athletes):
        athlete['position'] = i+1

    def wrap_data_to_datatables(athletes_list):
        return dict(aaData=sorted_athletes)

    return HttpResponse(simplejson.dumps(wrap_data_to_datatables(athletes_list)),
                        content_type="application/json")


@require_http_methods(['GET'])
def ranking_html(request, modality_type, gender, category):
    modality = Modality.objects.get(abbreviation=modality_type)
    events = list(Event.objects.filter(type__modality=modality).order_by('ocurring_at'))
    last_col_index = len(events) + 3
    return _rr(request, "ranking.html", {'modality_type': modality_type,
                                         'events': events,
                                         'gender': gender,
                                         'category': category,
                                         "last_col_index": last_col_index})

def all_rankings(request, modality_type, gender):
    modality = get_object_or_404(Modality, abbreviation=modality_type)
    categories = [category for category in Category.objects.all() if category.has_athlete(gender=gender, modality=modality)]
    events = list(Event.objects.filter(type__modality=modality))
    last_col_index = len(events) + 3
    return _rr(request, "ranking_all.html", {"modality_type":modality_type,
                                             "gender": gender,
                                             "events": events,
                                             "categories": categories,
                                             "last_col_index": last_col_index})
