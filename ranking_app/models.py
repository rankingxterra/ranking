# -*- coding: utf-8 -*-
from os.path import join

from datetime import datetime

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _


class Modality(models.Model):
    name = models.CharField(_(_(u'Nome')), max_length=64)
    abbreviation = models.CharField(_(u'Abreviatura'), max_length=3, unique=True)

    class Meta:
        verbose_name = _(u'Modalidade')
        verbose_name_plural = _(u'Modalidades')

    def __unicode__(self):
        return self.name


class EventType(models.Model):
    name = models.CharField(_(u'Nome'), max_length=32)
    modality = models.ForeignKey(Modality)

    class Meta:
        verbose_name = _(u'Tipo de Etapa')
        verbose_name_plural = _(u'Tipos de Etapa')

    def __unicode__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(_(u'Nome'),max_length=64)
    type = models.ForeignKey(EventType)
    ocurring_at = (models.DateField(_(u'Quando ocorre')))

    class Meta:
        verbose_name = _(u'Etapa')
        verbose_name_plural = _(u'Etapas')

    def __unicode__(self):
        return self.name


class CsvUploadedFile(models.Model):
    csv_file = models.FileField(_(u'Arquivo CSV'), upload_to='event_rankings')
    event = models.ForeignKey(Event)
    processed = models.NullBooleanField(_(u'Processado'))
    lines_processed = models.IntegerField(default=0)
    errors = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _(u'Upload de Arquivo')
        verbose_name_plural = _(u'Uploads de Arquivo')

    def __unicode__(self):
        return '%s for %s' % (self.csv_file.name, self.event.name)


class Category(models.Model):
    name = models.CharField(_(u'Nome'), max_length=64)
    abbreviation = models.CharField(_(u'Abreviatura'), max_length=2, unique=True)

    class Meta:
        verbose_name = u'Categoria'

    def __unicode__(self):
        return u'%s' % self.name

    def has_athlete(self, gender=None, modality=None):
        positions = AthleteEventPosition.objects.filter(
            event__type__modality=modality,
            athlete__gender=gender,
            athlete__disqualified=False,
            athlete__category=self
        )

        return bool(positions)


class Athlete(models.Model):
    GENDER_CHOICES = (
        ('M', 'Masculino'),
        ('F', 'Feminino'),
    )

    name = models.CharField(_(u'Nome'), max_length=64)
    xterra_id = models.CharField(_(u'ID'), max_length=15, unique=True)
    gender = models.CharField(_(u'Gênero'),  choices=GENDER_CHOICES, max_length=1)
    email = models.EmailField(_(u'Email'), max_length=254, null=True, blank=True)
    category = models.ForeignKey(Category)
    disqualified = models.BooleanField(_(u'Desclassificado'), default=False)
    team_name = models.CharField(_(u'Equipe'), max_length=100)

    class Meta:
        verbose_name = _(u'Atleta')
        verbose_name_plural = _(u'Atletas')

    def __unicode__(self):
        return u'%s: %s' % (self.name, self.xterra_id)

    def total_points(self, events, modality):
        pontuations = {}
        date_fmt = "%d/%m/%Y"
        _f = lambda x: x.strftime(date_fmt)

        for event in events:
            pontuations[event.id] = dict(event_name=event.name,
                                         position=0,
                                         pontuation=0)

        all_positions = AthleteEventPosition.objects.filter(athlete=self)
        total_points = 0

        for position in all_positions:
            if position.event in events:
                current_event = position.event

                pontuation = 0
                try:
                    pontuation = PositionPontuation.objects.get(
                        event_type=current_event.type, position=position.position
                    ).pontuation
                except:
                    pass

                if 'created_at' in pontuations[current_event.id] and \
                    position.upload.created_at > pontuations[current_event.id]['o_created_at']:

                    # if we find a more recent upload, we must subtract previous
                    # pontuation and sum with the new one
                    total_points -= pontuations[current_event.id]['pontuation']
                    total_points += pontuation

                    pontuations[current_event.id]['pontuation'] = pontuation
                    pontuations[current_event.id]['created_at'] = _f(position.upload.created_at)
                    pontuations[current_event.id]['o_created_at'] = position.upload.created_at
                    pontuations[current_event.id]['position'] = position.position
                elif 'created_at' in pontuations[current_event.id] and \
                     position.upload.created_at < pontuations[current_event.id]['o_created_at']:
                    pass
                else:
                    pontuations[current_event.id]['pontuation'] = pontuation
                    pontuations[current_event.id]['position'] = position.position
                    pontuations[current_event.id]['created_at'] = _f(position.upload.created_at)
                    pontuations[current_event.id]['o_created_at'] = position.upload.created_at
                    total_points += pontuation

        for value in pontuations.values():
            if 'o_created_at' in value:
                del value['o_created_at']

        return (pontuations, total_points)


class AthleteEventPosition(models.Model):
    event = models.ForeignKey(Event)
    athlete = models.ForeignKey(Athlete)
    position = models.IntegerField()
    upload = models.ForeignKey(CsvUploadedFile)

    class Meta:
        verbose_name = _(u'Posição de Atleta em Evento')
        verbose_name_plural = _(u'Posições de Atletas em Eventos')

    def __unicode__(self):
        return u'%s: %s #%d' % (self.event.name, self.athlete.name, self.position)


class PositionPontuation(models.Model):
    event_type = models.ForeignKey(EventType)
    position = models.IntegerField()
    pontuation = models.IntegerField()

    class Meta:
        verbose_name = _(u'Pontos por Posição')
        verbose_name_plural = _(u'Pontos por Posições')

    def __unicode__(self):
        return u'Tp. Evento: %s Posição: %d Pontos: %d' % (self.event_type.name,
                                                          self.position,
                                                          self.pontuation)
