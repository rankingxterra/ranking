# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Modality'
        db.create_table(u'ranking_app_modality', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
        ))
        db.send_create_signal(u'ranking_app', ['Modality'])

        # Adding model 'EventType'
        db.create_table(u'ranking_app_eventtype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('modality', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.Modality'])),
        ))
        db.send_create_signal(u'ranking_app', ['EventType'])

        # Adding model 'Event'
        db.create_table(u'ranking_app_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.EventType'])),
        ))
        db.send_create_signal(u'ranking_app', ['Event'])

        # Adding model 'CsvUploadedFile'
        db.create_table(u'ranking_app_csvuploadedfile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('csv_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.Event'])),
            ('processed', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('lines_processed', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('errors', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'ranking_app', ['CsvUploadedFile'])

        # Adding model 'Category'
        db.create_table(u'ranking_app_category', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(unique=True, max_length=2)),
        ))
        db.send_create_signal(u'ranking_app', ['Category'])

        # Adding model 'Athlete'
        db.create_table(u'ranking_app_athlete', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('cpf', self.gf('django.db.models.fields.CharField')(unique=True, max_length=15)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=254, null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.Category'])),
            ('disqualified', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'ranking_app', ['Athlete'])

        # Adding model 'AthleteEventPosition'
        db.create_table(u'ranking_app_athleteeventposition', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.Event'])),
            ('athlete', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.Athlete'])),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
            ('upload', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.CsvUploadedFile'])),
        ))
        db.send_create_signal(u'ranking_app', ['AthleteEventPosition'])

        # Adding model 'PositionPontuation'
        db.create_table(u'ranking_app_positionpontuation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('event_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['ranking_app.EventType'])),
            ('position', self.gf('django.db.models.fields.IntegerField')()),
            ('pontuation', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'ranking_app', ['PositionPontuation'])


    def backwards(self, orm):
        # Deleting model 'Modality'
        db.delete_table(u'ranking_app_modality')

        # Deleting model 'EventType'
        db.delete_table(u'ranking_app_eventtype')

        # Deleting model 'Event'
        db.delete_table(u'ranking_app_event')

        # Deleting model 'CsvUploadedFile'
        db.delete_table(u'ranking_app_csvuploadedfile')

        # Deleting model 'Category'
        db.delete_table(u'ranking_app_category')

        # Deleting model 'Athlete'
        db.delete_table(u'ranking_app_athlete')

        # Deleting model 'AthleteEventPosition'
        db.delete_table(u'ranking_app_athleteeventposition')

        # Deleting model 'PositionPontuation'
        db.delete_table(u'ranking_app_positionpontuation')


    models = {
        u'ranking_app.athlete': {
            'Meta': {'object_name': 'Athlete'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Category']"}),
            'cpf': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '15'}),
            'disqualified': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '254', 'null': 'True', 'blank': 'True'}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'ranking_app.athleteeventposition': {
            'Meta': {'object_name': 'AthleteEventPosition'},
            'athlete': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Athlete']"}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {}),
            'upload': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.CsvUploadedFile']"})
        },
        u'ranking_app.category': {
            'Meta': {'object_name': 'Category'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'ranking_app.csvuploadedfile': {
            'Meta': {'object_name': 'CsvUploadedFile'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'csv_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'errors': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lines_processed': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'processed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'ranking_app.event': {
            'Meta': {'object_name': 'Event'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.EventType']"})
        },
        u'ranking_app.eventtype': {
            'Meta': {'object_name': 'EventType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modality': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.Modality']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        u'ranking_app.modality': {
            'Meta': {'object_name': 'Modality'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'ranking_app.positionpontuation': {
            'Meta': {'object_name': 'PositionPontuation'},
            'event_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['ranking_app.EventType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pontuation': ('django.db.models.fields.IntegerField', [], {}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['ranking_app']