# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _

from ranking_app.models import CsvUploadedFile

class RankingUploadForm(forms.ModelForm):
    class Meta:
        model = CsvUploadedFile
        fields = ('csv_file', 'event')

    def __init__(self, *args, **kwargs):
        super(RankingUploadForm, self).__init__(*args, **kwargs)
        self.fields['event'].label = _(u'Etapa')

    def clean_csv_file(self):
        file = self.cleaned_data['csv_file']
        if not file.name.split('.')[-1] == 'csv':
            raise forms.ValidationError(_(u'Tipo de arquivo não suportado.'))

        return file
